#include <iostream>

#include <boost/locale/generator.hpp>
#include <tgbot/tgbot.h>

#include "env_wrapper.hpp"
#include "TgBot_wrapper.hpp"

#include "database/commands.hpp"
#include "database/database.hpp"


#define IGNORE_IF_INVALID(bot, msg) \
    if (is_invalid_input(bot, msg)) return;


bool is_invalid_input(const TgBot_wrapper &bot, TgBot::Message::Ptr msg)
{
    static std::string valid_symb ="/=_ .";
    bool is_valid = std::all_of(msg->text.begin(), msg->text.end(), [&](const char c) {
        return  (std::isalnum(c) && c != '\x7f') || valid_symb.find(c) != std::string::npos;
    });

    if (!is_valid) {
        bot.getApi().sendMessage(msg->chat->id, "Using unicode symbols is forbidden. ");
    }

    return !is_valid;
}



int main()
{
    boost::locale::generator gen;
    std::locale loc = gen("de_DE.UTF-8");
    std::locale::global(loc);

    const std::string bot_token(safe_getenv("TGBOT_TOKEN"));
    TgBot_wrapper bot(bot_token);

    /* START user table commands */
    bot.set_on_cmd(commands::misc::ping(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        bot.getApi().sendMessage(msg->chat->id, "pong");

    }).set_on_cmd(commands::user::record(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = users::record_if_not_exists(bot, msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::user::remove(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = users::remove_if_exists(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::user::setname(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = users::set_name(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::user::balance(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = users::balance(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::user::account(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = users::account(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());
    /* END user table commands */
    /* START box table commands */
    }).set_on_cmd(commands::box::deposit(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = box::deposit(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::box::withdraw(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = box::withdraw_if_exists(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::box::list(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = box::list(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    /* END box table commands */
    /* START pfand table commands */
    }).set_on_cmd(commands::pfand::record(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = pfand::insert(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::pfand::remove(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = pfand::remove_if_exists(msg, commands::pfand::remove());
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());

    }).set_on_cmd(commands::pfand::list(), [&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        query_r res = pfand::list(msg);
        bot.getApi().sendMessage(msg->chat->id, res.get_exit_result());
    /* END pfand table commands */
    }).set_on_noncmd_msg([&bot](TgBot::Message::Ptr msg) {
        IGNORE_IF_INVALID(bot, msg)

        bot.getApi().sendMessage(msg->chat->id, "unrecognised: " + msg->text);

    }).start();

    return 0;
}
