//
// Created by gesus on 10.03.18.
//

#include <boost/locale.hpp>
#include <map>
#include <iomanip>

#include <tgbot/types/Message.h>

#include <database/connection.hpp>
#include <database/impl.hpp>
#include <database/tables.hpp>

#include <helpers/commands.hpp>
#include <helpers/query_r.hpp>



query_r pfand::record(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return pfand::record(db, msg);
}


query_r pfand::record(db_conn &db, TgBot::Message::Ptr msg)
{
    auto is_valid_value = [](float f) -> bool {
        return f > 0 && std::ceil(f) == f;
    };

    const std::string record_cmd = commands::fmt(commands::pfand::record());
    kw_lookup parsed = commands::parse_flags(msg->text, {
            commands::__flag_key_pair("d=", "drink"),
            commands::__flag_key_pair("v=", "value")
    });

    if (parsed.empty()) {
        return query_r(POORLY_FMT_CMD, "usage: " + record_cmd + " d=drink name v=value");
    }

    query_r res = pfand::contains(db, parsed["drink"]);
    if (res.get_exit_code() == ITEM_IN_TBL) {
        return query_r(ITEM_IN_TBL, "error: drink " + parsed["drink"] + " is already in the pfand table");
    }

    float pfand_f;
    try {
        pfand_f = std::stof(parsed["value"]);
        std::cout << pfand_f << std::endl;
    }

    catch (std::exception& e) {
        return query_r(POORLY_FMT_CMD, "error: failed to parse " + parsed["value"]);
    }

    if (!is_valid_value(pfand_f)) {
        return query_r(POORLY_FMT_CMD, "error: the pfand value must be > 0 and be accurate to the hundredth decimal point");
    }

    auto pfand_v = static_cast<uint64_t>(std::round(pfand_f * 100));

    tables::pfand pfand_tbl;
    auto prepared_insert = db.prepare(insert_into(pfand_tbl).set(
            pfand_tbl.name = boost::locale::to_lower(parsed.at("drink")),
            pfand_tbl.value = pfand_v
    ));

    db(prepared_insert);

    const db_action action = db_action::builder()
        .uid(msg->chat->id)
        .pname(parsed.at("drink"))
        .value(pfand_v)
        .build();

    actions::log(db, action);

    return query_r(SUCCESS, "Successfully saved new drink:\n" + std::accumulate(
            parsed.begin(), parsed.end(),
            std::string(), join_on_token(", ", kw_to_str)) + "€"
    );
}


query_r pfand::contains(const std::string& drink)
{
    db_conn db = init_debug_db_conn();
    return pfand::contains(db, drink);
}


query_r pfand::contains(db_conn& db, const std::string& drink)
{
    tables::pfand pfand_tbl;
    const std::string drink_tolower = boost::locale::to_lower(drink);
    auto prepared_select = db.prepare(select(pfand_tbl.name)
            .from(pfand_tbl)
            .where(pfand_tbl.name == drink_tolower
    ));

    auto pfand_tuples = db(prepared_select);

    switch (auto matching_names = query_result_size(pfand_tuples)) {
        case 0:
            return query_r(ITEM_NOT_IN_TBL, "could not find drink=" + drink);

        case 1:
            return query_r(ITEM_IN_TBL, "drink=" + drink + " exists exactly once");

        default:
            return query_r(ITEM_DUPL_IN_TBL, "drink=" + drink + " exists " + std::to_string(matching_names) + " times");
    }
}


query_r pfand::remove_if_exists(TgBot::Message::Ptr msg, const std::string& cmd)
{
    db_conn db = init_debug_db_conn();
    return pfand::remove_if_exists(db, msg, cmd);
}


query_r pfand::remove_if_exists(db_conn &db, TgBot::Message::Ptr msg, const std::string& cmd) {
    const std::string cmd_fmt = commands::fmt(cmd);
    kw_lookup parsed = commands::parse_flags(msg->text, {
            commands::__flag_key_pair("d=", "drink")
    });
    if (parsed.empty()) {
        return query_r(POORLY_FMT_CMD, "usage: " + cmd_fmt + " d=drink name");
    }

    query_r res = pfand::contains(db, parsed.at("drink"));
    const db_action action = db_action::builder()
            .uid(msg->chat->id)
            .pname(parsed.at("drink"))
            .build();

    switch (res.get_exit_code()) {
        case ITEM_NOT_IN_TBL:
            return res;

        case ITEM_IN_TBL:
            tables::pfand pfand_tbl;
            db(db.prepare(remove_from(pfand_tbl)
                .where(pfand_tbl.name == boost::locale::to_lower(parsed["drink"]))
            ));

            actions::log(db, action);

            return query_r(SUCCESS, "successfully removed " + parsed["drink"] + " from pfand table");

        default:
            return query_r(UNKNOWN_ERROR, "unexpected return value: " + std::to_string(res.get_exit_code()));
    }
}


query_r pfand::list(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return pfand::list(db, msg);
}


query_r pfand::list(db_conn& db, TgBot::Message::Ptr msg)
{
    tables::pfand pfand_tbl;
    auto sorted_pfand_tuples = db(
            select(all_of(pfand_tbl))
                    .from(pfand_tbl)
                    .unconditionally()
                    .order_by(pfand_tbl.name.asc())
    );

    static constexpr auto format_pfand_tuple = [](const auto& pfand) -> std::string {
        std::stringstream ss;
        ss << boost::locale::to_title(std::string(pfand.name.text)) << ": " << std::fixed << std::setprecision(2) << "0." << pfand.value << "€";
        return ss.str();
    };

    const std::string fmt_table = std::accumulate(
            sorted_pfand_tuples.begin(), sorted_pfand_tuples.end(),
            std::string(), join_on_token("\n", format_pfand_tuple)
    );


    return fmt_table.empty()
           ? query_r(EMPTY_TBL, "The pfand table is empty")
           : query_r(SUCCESS, fmt_table);
}