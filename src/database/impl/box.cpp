//
// Created by gesus on 10.03.18.
//

#include <iomanip>
#include <boost/locale.hpp>

#include <tgbot/types/Message.h>

#include <database/connection.hpp>
#include <database/impl.hpp>
#include <helpers/query_r.hpp>
#include <database/tables.hpp>
#include <helpers/commands.hpp>


query_r box::contains(const std::string& drink)
{
    db_conn db = init_debug_db_conn();
    return box::contains(db, drink);
}


query_r box::contains(db_conn& db, const std::string& drink)
{
    tables::box box_tbl;
    const std::string drink_tolower = boost::locale::to_lower(drink);

    auto prepared_select = db.prepare(
            select(box_tbl.name)
            .from(box_tbl)
            .where(box_tbl.name == drink_tolower)
    );

    auto box_tuples = db(prepared_select);

    switch (auto matching_names = query_result_size(box_tuples)) {
        case 0:
            return query_r(ITEM_NOT_IN_TBL, "could not find drink=" + drink);

        case 1:
            return query_r(ITEM_IN_TBL, "drink=" + drink + " exists exactly once");

        default:
            return query_r(ITEM_DUPL_IN_TBL, "drink=" + drink + " exists " + std::to_string(matching_names) + " times");
    }
}


query_r box::deposit(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return box::deposit(db, msg);
}


query_r initial_deposit(db_conn& db, const std::string& drink, const long long quantity)
{
    tables::box box_tbl;
    auto prepared_insert = db.prepare(insert_into(box_tbl).set(
            box_tbl.name = boost::locale::to_lower(drink),
            box_tbl.quantity = quantity
    ));

    db(prepared_insert);

    return query_r(SUCCESS, "Successfully deposited into box:\n" + std::to_string(quantity) + " " + drink);
}


query_r update_deposit(db_conn& db, const std::string& drink, const long long quantity)
{
    tables::box box_tbl;
    auto prepared_update = db.prepare(update(box_tbl)
            .set(box_tbl.quantity += quantity)
            .where(box_tbl.name == boost::locale::to_lower(drink))
    );
    db(prepared_update);

    return query_r(SUCCESS, "Successfully deposited into box:\n" + std::to_string(quantity) + " " + drink);
}


query_r box::deposit(db_conn& db, TgBot::Message::Ptr msg)
{
    const std::string record_cmd = commands::fmt(commands::box::deposit());
    kw_lookup parsed = commands::parse_flags(msg->text, {
            commands::__flag_key_pair("d=", "drink"),
            commands::__flag_key_pair("n=", "quantity")
    });

    if (parsed.empty()) {
        return query_r(POORLY_FMT_CMD, "usage: " + record_cmd + " d=drink name n=quantity");
    }

    query_r pfand_res = pfand::contains(db, parsed["drink"]);
    if (pfand_res.get_exit_code() == ITEM_NOT_IN_TBL) {
        return query_r(ITEM_NOT_IN_TBL, "error: drink " + parsed["drink"] + " has not been recorded in the pfand table");
    }

    query_r box_res = box::contains(db, parsed["drink"]);

    long long quantity;
    try {
        quantity = std::stoll(parsed["quantity"]);
    }

    catch (std::exception& e) {
        return query_r(POORLY_FMT_CMD, "error: failed to parse " + parsed["quantity"]);
    }

    if (quantity <= 0) {
        return query_r(POORLY_FMT_CMD, "error: the quantity must be > 0");
    }

    const db_action action = db_action::builder()
            .pname(parsed.at("drink"))
            .quantity(quantity)
            .uid(msg->chat->id)
            .build();

    switch (box_res.get_exit_code()) {
        case ITEM_NOT_IN_TBL:
            return initial_deposit(db, parsed["drink"], quantity);

        case ITEM_IN_TBL:
            actions::log(db, action);
            return update_deposit(db, parsed["drink"], quantity);

        default:
            return query_r(UNKNOWN_ERROR, "unexpected return value: " + std::to_string(box_res.get_exit_code()));
    }
}


query_r withdraw_all(db_conn& db, const std::string& drink, const long long quantity)
{
    tables::box box_tbl;
    db(remove_from(box_tbl)
            .where(box_tbl.name == boost::locale::to_lower(drink) and box_tbl.quantity == quantity)
    );

    return query_r(SUCCESS, "Withdrew all " + drink + "(s)");
}


query_r withdraw_some(db_conn& db, const std::string& drink, const long long quantity)
{
    tables::box box_tbl;
    db(update(box_tbl)
            .set(box_tbl.quantity -= quantity)
            .where(box_tbl.name == boost::locale::to_lower(drink))
    );

    return query_r(SUCCESS, "Withdrew " + std::to_string(quantity) + " " + drink + "(s)");
}


query_r box::withdraw_if_exists(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return box::withdraw_if_exists(db, msg);
}


query_r box::withdraw_if_exists(db_conn &db, TgBot::Message::Ptr msg)
{
    const std::string withdraw_cmd = commands::fmt(commands::box::withdraw());
    kw_lookup parsed = commands::parse_flags(msg->text, {
            commands::__flag_key_pair("d=", "drink"),
            commands::__flag_key_pair("n=", "quantity")
    });

    if (parsed.empty()) {
        return query_r(POORLY_FMT_CMD, "usage: " + withdraw_cmd + " d=drink name n=quantity");
    }

    query_r pfand_res = pfand::contains(db, parsed["drink"]);
    if (pfand_res.get_exit_code() == ITEM_NOT_IN_TBL) {
        return query_r(ITEM_NOT_IN_TBL, "error: drink " + parsed["drink"] + " has not been recorded in the pfand table");
    }

    query_r box_res = box::contains(db, parsed["drink"]);
    if (box_res.get_exit_code() == ITEM_NOT_IN_TBL) {
        return query_r(ITEM_NOT_IN_TBL, "error: drink " + parsed["drink"] + " is not in the box");
    }

    long long withdraw_count;
    try {
        withdraw_count = std::stoll(parsed["quantity"]);
    }

    catch (std::exception& e) {
        return query_r(POORLY_FMT_CMD, "error: failed to parse " + parsed["quantity"]);
    }

    if (withdraw_count <= 0) {
        return query_r(POORLY_FMT_CMD, "error: the quantity must be > 0");
    }

    tables::box box_tbl;
    long long drink_count = db(select(box_tbl.quantity)
        .from(box_tbl).where(box_tbl.name == boost::locale::to_lower(parsed["drink"])
    )).front().quantity;

    if (drink_count < withdraw_count) {
        return query_r(INDEX_OUT_OF_BOUNDS, "error: you requested to withdraw " + parsed["quantity"] + " " + parsed["drink"] +
                "(s) but there are only " + std::to_string(drink_count) + " in the box");
    }

    const db_action action = db_action::builder()
            .action(actions::BOX_WITHDRAW())
            .pname(parsed.at("drink"))
            .quantity(withdraw_count)
            .uid(msg->chat->id)
            .build();

    actions::log(db, action);

    if (drink_count == withdraw_count) {
        return withdraw_all(db, parsed["drink"], withdraw_count);
    }

    else {
        return withdraw_some(db, parsed["drink"], withdraw_count);
    }
}


query_r box::list(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return box::list(db, msg);
}


query_r box::list(db_conn &db, TgBot::Message::Ptr msg)
{
    tables::box box_tbl;
    auto sorted_box_tuples = db(
            select(all_of(box_tbl))
                    .from(box_tbl)
                    .unconditionally()
                    .order_by(box_tbl.name.asc())
    );

    static constexpr auto format_box_tuple = [](auto& box_content) -> std::string {
        return std::to_string(box_content.quantity) + " " + boost::locale::to_title(box_content.name.text);
    };

    std::string fmt_table = std::accumulate(sorted_box_tuples.begin(), sorted_box_tuples.end(), std::string(),
                                            join_on_token("\n", format_box_tuple));

    return fmt_table.empty()
            ? query_r(EMPTY_TBL, "The box is empty")
            : query_r(SUCCESS, fmt_table);
}