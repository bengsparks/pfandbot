//
// Created by gesus on 10.03.18.
//


#include <helpers/query_r.hpp>
#include <database/connection.hpp>
#include <tgbot/types/Message.h>
#include <database/impl.hpp>
#include <database/tables.hpp>

query_r exists(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return users::exists(db, msg);
}


query_r users::exists(db_conn& db, TgBot::Message::Ptr msg)
{
    tables::users users;

    auto tuples = db(select(all_of(users)).from(users).where(users.id == msg->chat->id));

    switch (auto matching_ids = query_result_size(tuples)) {
        case 0:
            return query_r(USER_DOES_NOT_EXIST, std::string("could not find id=") + std::to_string(msg->chat->id));

        case 1:
            return query_r(USER_DOES_EXIST, std::string("id=") + std::to_string(msg->chat->id) + std::string(" exists exactly once"));

        default:
            return query_r(ITEM_DUPL_IN_TBL, std::string("id=") + std::to_string(msg->chat->id) + " exists " + std::to_string(matching_ids) + " times");
    }
}


query_r users::record_if_not_exists(TgBot_wrapper &bot, TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return users::create_if_not_exists(bot, db, msg);
}


query_r users::create_if_not_exists(TgBot_wrapper& bot, db_conn &db, TgBot::Message::Ptr msg)
{
    query_r res = users::exists(db, msg);

    switch (res.get_exit_code()) {
        case USER_DOES_EXIST:
            return query_r(USER_DOES_EXIST, ("You (id=" + std::to_string(msg->chat->id) + ") are already registered"));

        case USER_DOES_NOT_EXIST:
            tables::users user_tbl;
            db(insert_into(user_tbl).set(
                    user_tbl.id = msg->chat->id,
                    user_tbl.name = "",
                    user_tbl.balance = 0
            ));

            return query_r(USER_DOES_NOT_EXIST, "You (id=" + std::to_string(msg->chat->id) + ") are now registered");


        default:
            return query_r(UNKNOWN_ERROR, "unexpected return value: " + std::to_string(res.get_exit_code()));
    }
}



query_r users::remove_if_exists(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return users::remove_if_exists(db, msg);
}


query_r users::remove_if_exists(db_conn &db, TgBot::Message::Ptr msg)
{
    query_r res = users::exists(db, msg);
    if (res.get_exit_code() == USER_DOES_EXIST) {
        tables::users user_tbl;
        db(remove_from(user_tbl).where(user_tbl.id == msg->chat->id));
        return query_r(SUCCESS, "Your user id '" + std::to_string(msg->chat->id) + "' has been removed");
    }

    return query_r(USER_DOES_NOT_EXIST, "Your user id '" + std::to_string(msg->chat->id) + "', is not recorded and therefore cannt be removed");
}

query_r users::account(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return users::account(db, msg);
}


query_r users::account(db_conn &db, TgBot::Message::Ptr msg)
{
    static constexpr auto user_tuple_fmt = [](auto& user_tuple) -> std::string {
        std::string name = std::strlen(user_tuple.name.text) == 0
                           ? "<empty>"
                           : user_tuple.name.text;

        std::vector<std::string> strs {
                std::string("id=") + std::to_string(user_tuple.id),
                std::string("name=") + name,
                std::string("balance=") + std::to_string(user_tuple.balance)
        };

        return std::accumulate(strs.begin(), strs.end(), std::string(), join_on_token(", ", identity));
    };


    query_r res = users::exists(db, msg);
    if (res.get_exit_code() == USER_DOES_EXIST) {
        tables::users user_tbl;
        std::string user_info;
        for (auto& user_tup : db(select(all_of(user_tbl)).from(user_tbl).where(user_tbl.id == msg->chat->id))) {
            user_info += user_tuple_fmt(user_tup);
        }
        return query_r(SUCCESS, user_info);
    }

    return res;
}


query_r users::set_name(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return users::set_name(db, msg);
}


query_r users::set_name(db_conn &db, TgBot::Message::Ptr msg)
{
    query_r res = users::exists(db, msg);
    if (res.get_exit_code() == USER_DOES_EXIST) {
        std::vector<std::string> parsed = split(msg->text, " ");

        if (parsed.size() < 2) {
            return query_r(POORLY_FMT_CMD, "usage: /user_setname <new_name>");
        }

        std::string new_name = std::accumulate(parsed.begin() + 1, parsed.end(), std::string(), join_on_token(" ", identity));
        tables::users user_tbl;
        auto prepared_insert = db.prepare((update(user_tbl)
                   .set(user_tbl.name = new_name)
                   .where(user_tbl.id == msg->chat->id)
        ));

        db(prepared_insert);

        return query_r(SUCCESS, std::string("Successfully changed your (id=" + std::to_string(msg->chat->id) + ") name to '" + new_name + "'"));
    }

    return res;
}



query_r users::balance(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return users::balance(db, msg);
}


query_r users::balance(db_conn &db, TgBot::Message::Ptr msg) {
    static constexpr auto user_tuple_cred_fmt = [](auto &user_tuple) -> std::string {
        return std::string("You (") + user_tuple.name.text +
               std::string(") have €") + std::to_string(user_tuple.balance) +
               std::string(" on your account");
    };

    query_r res = users::exists(db, msg);
    if (res.get_exit_code() == USER_DOES_EXIST) {
        tables::users user_tbl;
        std::string user_info;
        for (auto &user_tup : db(select(user_tbl.name, user_tbl.balance).from(user_tbl).where(user_tbl.id == msg->chat->id))) {
            user_info += user_tuple_cred_fmt(user_tup);
        }
        return query_r(SUCCESS, user_info);
    }

    return res;
}


query_r users::update_balance(TgBot::Message::Ptr msg)
{
    db_conn db = init_debug_db_conn();
    return users::update_balance(db, msg);
}


query_r users::update_balance(db_conn &db, TgBot::Message::Ptr msg)
{
    return query_r(SUCCESS, "called update_balance");
}