//
// Created by gesus on 20.03.18.
//

#include <boost/locale.hpp>
#include <string>

#include <database/connection.hpp>
#include <database/tables.hpp>
#include <database/impl.hpp>

#include <helpers/query_r.hpp>
#include <helpers/db_action.hpp>


template <typename T, typename R>
auto optional_to_db_val(const std::optional<T>& opt, const R& on_error) -> T {
    return (opt == std::nullopt) ? on_error : *opt;
}

template <typename T>
auto optional_to_db_val(const std::optional<T>& opt) -> T {
    return optional_to_db_val(opt, T());
}


query_r actions::log(db_conn &db, const db_action &act)
{
    std::cerr << act.to_str() << std::endl;

    const int64_t uid = optional_to_db_val(act.get_uid());
    const int64_t quantity = optional_to_db_val(act.get_quantity());

    const uint64_t value = optional_to_db_val(act.get_value());
    const uint64_t valdiff = optional_to_db_val(act.get_valdiff());

    const std::string pname = optional_to_db_val(act.get_pname());
    const std::string action = optional_to_db_val(act.get_action());


    tables::actions actions_tbl;
    db(insert_into(actions_tbl).set(
            actions_tbl.uid = uid,
            actions_tbl.twhen = sqlpp::chrono::floor<::std::chrono::milliseconds>(std::chrono::system_clock::now()),
            actions_tbl.action = action,
            actions_tbl.pname = boost::locale::to_lower(pname),

            actions_tbl.value = sqlpp::tvin(value),
            actions_tbl.quantity = sqlpp::tvin(quantity),
            actions_tbl.val_diff = sqlpp::tvin(valdiff)
    ));

    return query_r(SUCCESS, "successfully logged transaction");
}