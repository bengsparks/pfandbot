//
// Created by gesus on 10.03.18.
//

#include <memory>


#include "database/connection.hpp"
#include "helpers/env_wrapper.hpp"


static db_conn __init_db_conn(std::string host, std::string db_name, std::string user, std::string pw, bool debug)
{
    auto config = std::make_shared<sqlpp::postgresql::connection_config>();

    config->dbname = std::move(db_name);
    config->user = std::move(user);
    config->password = std::move(pw);
    config->host = std::move(host);

    config->debug = debug;

    db_conn db(config);
    return db;
}


db_conn init_conn()
{
    return __init_db_conn(
            safe_getenv("DB_HOST"),
            safe_getenv("DB_NAME"),
            safe_getenv("DB_USER"),
            safe_getenv("DB_PW"),
            false
    );
}


db_conn init_debug_db_conn()
{
    return __init_db_conn(
            safe_getenv("DB_HOST"),
            safe_getenv("DB_NAME"),
            safe_getenv("DB_USER"),
            safe_getenv("DB_PW"),
            true
    );
}


