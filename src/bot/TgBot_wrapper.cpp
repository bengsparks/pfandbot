//
// Created by gesus on 08.03.18.
//

#include "TgBot_wrapper.hpp"

#include <iostream>

TgBot_wrapper::TgBot_wrapper() :
    TgBot_wrapper("")
{ };


TgBot_wrapper::TgBot_wrapper(std::string token) :
    TgBot::Bot(token)
{ };


bool TgBot_wrapper::is_cmd(const std::string &msgText)
{
    return std::find_if(commands.begin(), commands.end(), [&msgText](std::string command) {
        return StringTools::startsWith(msgText, command);
    }) != commands.end();
}


void TgBot_wrapper::start()
{
    try {
        std::cout << "Bot Username: " << getApi().getMe()->username << std::endl;
        TgBot::TgLongPoll longPoll(*this);
        while (true) {
            std::cout << "long poll started" << std::endl;
            longPoll.start();
        }
    }

    catch (TgBot::TgException& exp) {
        std::cerr << "error: " << exp.what() << std::endl;
    }
}



TgBot_wrapper& TgBot_wrapper::set_on_cmd(const std::string &cmd, const TgBot::EventBroadcaster::MessageListener& listener)
{
    if (std::find(commands.begin(), commands.end(), cmd) == commands.end()) {
        getEvents().onCommand(cmd, listener);
        commands.push_back(cmd);
        std::cout << "registered command: " << cmd << std::endl;
    }

    return *this;
}



TgBot_wrapper& TgBot_wrapper::set_on_noncmd_msg(const TgBot::EventBroadcaster::MessageListener& listener)
{
    getEvents().onNonCommandMessage(listener);
    return *this;
}