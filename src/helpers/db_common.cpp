//
// Created by gesus on 15.03.18.
//

#include <boost/algorithm/string.hpp>

#include "helpers/db_common.hpp"


auto split(const std::string& str, const std::string& delim) -> std::vector<std::string> {
    std::vector<std::string> parsed;
    boost::split(parsed, str, boost::is_any_of(" "));
    return parsed;
};


auto split(const std::string& str, const char* delim) -> std::vector<std::string> {
    std::string __delim = std::string(delim);
    return split(str, __delim);
};


auto remove_extra_ws(const std::string& text) -> std::string {
    std::string ws_removed;
    bool prev_char_is_space = false;

    std::copy_if(text.begin(),text.end(), std::back_inserter(ws_removed), [&](const char c) {
        const bool ret = !prev_char_is_space || c != ' ';
        prev_char_is_space = c == ' ';
        return ret;
    });

    return ws_removed;
};

