//
// Created by gesus on 12.03.18.
//


#include <algorithm>
#include <sstream>

#include <helpers/commands.hpp>
#include <helpers/db_common.hpp>


const std::string commands::fmt(const std::string &cmd)
{
    return "/" + cmd;
}

auto kw_to_str(const std::pair<kw_lookup::key_type, const kw_lookup::mapped_type> &elem) -> std::string {
    std::stringstream ss;
    ss << elem.first << ": " << elem.second;
    return ss.str();
};


kw_lookup commands::parse_flags(const std::string &text, const std::vector<__flag_key_pair> &fkps)
{
    if (fkps.empty()) {
        return { };
    }

    struct __flag_pos_pair : public std::pair<std::string, std::string::size_type> {
        __flag_pos_pair(std::string flag, std::string::size_type pos) :
                std::pair<std::string, std::string::size_type>(flag, pos)
        { }

        auto flag() -> decltype(auto) { return first; }
        auto position() -> decltype(auto) { return second; }

        auto flag() const -> decltype(auto) { return first; }
        auto position() const -> decltype(auto) { return second; }

        bool operator <(const __flag_pos_pair& other) { return position() < other.position(); }
    };

    const std::string ws_removed = remove_extra_ws(text);
    std::vector<__flag_pos_pair> flag_positions;

    // create vector with flags and their corresponding positions
    for (const __flag_key_pair &fkp : fkps) {
        std::string::size_type flag_pos = __find_position(fkp.flag(), ws_removed);
        if (flag_pos == std::string::npos) {
            return { };
        }

        flag_positions.emplace_back(__flag_pos_pair(fkp.flag(), flag_pos));
    }

    if (!std::is_sorted(flag_positions.begin(), flag_positions.end())) {
        return { };
    }

    // place dummy node at end to avoid out of bounds
    flag_positions.emplace_back(__flag_pos_pair("", ws_removed.length() + 1));

    kw_lookup kw_args;
    std::vector<__flag_pos_pair>::size_type i = 0;
    for (const auto& fkp : fkps) {
        kw_args[fkp.key()] = __extract_flag_value(
                ws_removed, fkp.flag(), flag_positions[i].position(), flag_positions[i + 1].position() - 1
        );
        ++i;
    }

    return kw_args;
}


const unsigned long commands::__find_position(const std::string &needle, const std::string &haystack) {
    return haystack.find(needle);
};


const std::string commands::__extract_flag_value(const std::string &str, const std::string &flag,
                                                 std::string::size_type from, std::string::size_type to) {
    from += flag.length();
    return str.substr(from, to - from);
};