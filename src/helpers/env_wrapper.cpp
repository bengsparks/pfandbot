//
// Created by gesus on 14.03.18.
//

#include <cstdlib>
#include <string>
#include <iostream>

#include "helpers/env_wrapper.hpp"


const std::string safe_getenv(const char *env_name)
{
    const char* getenv_res = std::getenv(env_name);
    if (!getenv_res) {
        std::cerr << "FATAL ERROR: " << env_name << " is not set as an environment variable" << std::endl;
        std::exit(1);
    }

    return std::string(getenv_res);
}