//
// Created by gesus on 23.03.18.
//

#include "helpers/db_action.hpp"

#include <sstream>

std::string db_action::to_str() const
{
    auto opt_to_str = [](const auto opt) -> std::string {
        std::stringstream ss;

        if (opt == std::nullopt)    ss << "std::nullopt";
        else                        ss << *opt;

        return ss.str();
    };

    return  std::string("action{") +
            "\nuid="        + opt_to_str(get_uid()) +
            "\naction="     + opt_to_str(get_action()) +
            "\npname="      + opt_to_str(get_pname()) +
            "\nvalue="      + opt_to_str(get_value()) +
            "\nquantity="   + opt_to_str(get_quantity()) +
            "\nval_diff="   + opt_to_str(get_valdiff()) +
            "}";
}