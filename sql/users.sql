CREATE TABLE IF NOT EXISTS users (
	id	    BIGINT          NOT NULL,
	name	VARCHAR(200)	NOT NULL,
	balance	BIGINT          NOT NULL,

	PRIMARY KEY(id)
);
