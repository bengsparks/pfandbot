CREATE TABLE IF NOT EXISTS box (
    name        VARCHAR(200)        NOT NULL,
    quantity    BIGINT              NOT NULL,

    FOREIGN KEY (name) REFERENCES pfand(name)
);
