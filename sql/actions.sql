CREATE TABLE IF NOT EXISTS actions (
	tid 	SERIAL,
	uid 	BIGINT   	            NOT NULL,
	pname	VARCHAR(200)	        NOT NULL,

	twhen	TIME WITH TIME ZONE	    NOT NULL,
	action	VARCHAR(10)	            NOT NULL,
	quantity BIGINT 	            NOT NULL,
	val_diff BIGINT                 NOT NULL,

	PRIMARY KEY (tid),
	FOREIGN KEY (uid) REFERENCES users(id),
	FOREIGN KEY (pname) REFERENCES pfand(name)
);
