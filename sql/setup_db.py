import os

def setup():
    os.system("sudo rc-service postgresql start")
    os.system("sudo -u postgres psql -a -f {}".format("pfand_credit.sql"))

    for filename in ("pfand", "box", "users", "actions"):
        os.system("sudo -u postgres psql -d pfand_credit -a -f {}".format(filename + ".sql"))


if __name__ == "__main__":
    setup()
