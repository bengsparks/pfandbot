CREATE TABLE IF NOT EXISTS pfand (
	name 		VARCHAR(200) 		NOT NULL,
	value		BIGINT 	            NOT NULL,

	PRIMARY KEY	(name)
);
