//
// Created by gesus on 10.03.18.
//

#ifndef PFANDKASTEN_CONNECTION_HPP
#define PFANDKASTEN_CONNECTION_HPP

#include <string>

#include <sqlpp11/postgresql/postgresql.h>
#include <sqlpp11/sqlpp11.h>

using db_conn = sqlpp::postgresql::connection;

db_conn init_debug_db_conn();
db_conn init_conn();

#endif //PFANDKASTEN_CONNECTION_HPP
