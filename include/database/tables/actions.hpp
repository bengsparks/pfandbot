#ifndef TABLES_ACTIONS_H
#define TABLES_ACTIONS_H

#include <sqlpp11/table.h>
#include <sqlpp11/char_sequence.h>
#include <sqlpp11/column_types.h>

namespace tables {

	namespace actions_ {

		struct Tid {
			struct _alias_t {
				static constexpr const char _literal[] = R"("tid")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T tid;
						T &operator()() { return tid; }
						const T &operator()() const { return tid; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::integer, sqlpp::tag::must_not_insert, sqlpp::tag::must_not_update>;
		};

		struct Uid {
			struct _alias_t {
				static constexpr const char _literal[] = R"("uid")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T uid;
						T &operator()() { return uid; }
						const T &operator()() const { return uid; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::bigint, sqlpp::tag::require_insert>;
		};

		struct Pname {
			struct _alias_t {
				static constexpr const char _literal[] = R"("pname")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T pname;
						T &operator()() { return pname; }
						const T &operator()() const { return pname; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::varchar, sqlpp::tag::require_insert>;
		};

		struct Twhen {
			struct _alias_t {
				static constexpr const char _literal[] = R"("twhen")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T twhen;
						T &operator()() { return twhen; }
						const T &operator()() const { return twhen; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::time_point, sqlpp::tag::require_insert>;
		};

		struct Value {
			struct _alias_t {
				static constexpr const char _literal[] = R"("value")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T value;
						T &operator()() { return value; }
						const T &operator()() const { return value; }
					};
				};

			using _traits = ::sqlpp::make_traits<::sqlpp::bigint, sqlpp::tag::require_insert>;
		};

		struct Action {
			struct _alias_t {
				static constexpr const char _literal[] = R"("action")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T action;
						T &operator()() { return action; }
						const T &operator()() const { return action; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::varchar, sqlpp::tag::require_insert>;
		};

		struct Quantity {
			struct _alias_t {
				static constexpr const char _literal[] = R"("quantity")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T quantity;
						T &operator()() { return quantity; }
						const T &operator()() const { return quantity; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::bigint, sqlpp::tag::require_insert>;
		};

		struct Val_diff {
			struct _alias_t {
				static constexpr const char _literal[] = R"("val_diff")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T val_diff;
						T &operator()() { return val_diff; }
						const T &operator()() const { return val_diff; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::bigint, sqlpp::tag::require_insert>;
		};
	}

	struct actions : sqlpp::table_t<actions,
				actions_::Tid,
				actions_::Uid,
				actions_::Pname,
				actions_::Twhen,
				actions_::Action,
				actions_::Value,
				actions_::Quantity,
				actions_::Val_diff> {
		using _value_type = sqlpp::no_value_t;
		struct _alias_t {
			static constexpr const char _literal[] = R"("actions")";
			using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
			template<typename T>
				struct _member_t {
					T actions;
					T &operator()() { return actions; }
					const T &operator()() const { return actions; }
				};
		};
	};
}

#endif
