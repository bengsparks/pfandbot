#ifndef TABLES_BOX_H
#define TABLES_BOX_H

#include <sqlpp11/table.h>
#include <sqlpp11/char_sequence.h>
#include <sqlpp11/column_types.h>

namespace tables {

	namespace box_ {

		struct Name {
			struct _alias_t {
				static constexpr const char _literal[] = R"("name")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T name;
						T &operator()() { return name; }
						const T &operator()() const { return name; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::varchar, sqlpp::tag::require_insert>;
		};

		struct Quantity {
			struct _alias_t {
				static constexpr const char _literal[] = R"("quantity")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T quantity;
						T &operator()() { return quantity; }
						const T &operator()() const { return quantity; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::bigint, sqlpp::tag::require_insert>;
		};
	}

	struct box : sqlpp::table_t<box,
				box_::Name,
				box_::Quantity> {
		using _value_type = sqlpp::no_value_t;
		struct _alias_t {
			static constexpr const char _literal[] = R"("box")";
			using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
			template<typename T>
				struct _member_t {
					T box;
					T &operator()() { return box; }
					const T &operator()() const { return box; }
				};
		};
	};
}

#endif
