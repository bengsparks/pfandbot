#ifndef TABLES_PFAND_H
#define TABLES_PFAND_H

#include <sqlpp11/table.h>
#include <sqlpp11/char_sequence.h>
#include <sqlpp11/column_types.h>

namespace tables {

	namespace pfand_ {

		struct Name {
			struct _alias_t {
				static constexpr const char _literal[] = R"("name")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T name;
						T &operator()() { return name; }
						const T &operator()() const { return name; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::varchar, sqlpp::tag::require_insert>;
		};

		struct Value {
			struct _alias_t {
				static constexpr const char _literal[] = R"("value")";
				using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
				template<typename T>
					struct _member_t {
						T value;
						T &operator()() { return value; }
						const T &operator()() const { return value; }
					};
			};

			using _traits = ::sqlpp::make_traits<::sqlpp::bigint, sqlpp::tag::require_insert>;
		};
	}

	struct pfand : sqlpp::table_t<pfand,
				pfand_::Name,
				pfand_::Value> {
		using _value_type = sqlpp::no_value_t;
		struct _alias_t {
			static constexpr const char _literal[] = R"("pfand")";
			using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
			template<typename T>
				struct _member_t {
					T pfand;
					T &operator()() { return pfand; }
					const T &operator()() const { return pfand; }
				};
		};
	};
}

#endif
