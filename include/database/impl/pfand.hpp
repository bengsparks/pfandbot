//
// Created by gesus on 10.03.18.
//

#ifndef PFANDKASTEN_PFAND_HPP
#define PFANDKASTEN_PFAND_HPP

#include <tgbot/types/Message.h>


namespace pfand {
    query_r record(TgBot::Message::Ptr msg);
    query_r record(db_conn &db, TgBot::Message::Ptr msg);

    query_r contains(const std::string& drink);
    query_r contains(db_conn& db, const std::string& drink);

    query_r remove_if_exists(TgBot::Message::Ptr msg, const std::string& cmd);
    query_r remove_if_exists(db_conn &db, TgBot::Message::Ptr msg, const std::string& cmd);

    query_r list(TgBot::Message::Ptr msg);
    query_r list(db_conn& db, TgBot::Message::Ptr msg);
};


#endif //PFANDKASTEN_PFAND_HPP
