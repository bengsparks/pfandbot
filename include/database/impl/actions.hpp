//
// Created by gesus on 20.03.18.
//

#ifndef PFANDBOT_ACTIONS_HPP
#define PFANDBOT_ACTIONS_HPP

#include <string>
#include <helpers/db_action.hpp>
#include <database/connection.hpp>
#include <helpers/query_r.hpp>


namespace actions {
    static constexpr auto PFAND_RECORD = []() -> std::string { return "pfand_record"; };
    static constexpr auto PFAND_REMOVE = []() -> std::string { return "pfand_remove"; };
    static constexpr auto BOX_DEPOSIT  = []() -> std::string { return "box_deposit"; };
    static constexpr auto BOX_WITHDRAW = []() -> std::string { return "box_withdraw"; };

    query_r log(db_conn &db, const db_action &act);
};

#endif //PFANDBOT_ACTIONS_HPP
