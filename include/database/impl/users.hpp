//
// Created by gesus on 10.03.18.
//

#ifndef PFANDKASTEN_USERS_HPP
#define PFANDKASTEN_USERS_HPP

#include <tgbot/types/Message.h>

#include <TgBot_wrapper.hpp>


namespace users {
    query_r exists(TgBot::Message::Ptr msg);
    query_r exists(db_conn& db, TgBot::Message::Ptr msg);

    query_r record_if_not_exists(TgBot_wrapper &bot, TgBot::Message::Ptr msg);
    query_r create_if_not_exists(TgBot_wrapper& bot, db_conn& db, TgBot::Message::Ptr msg);

    query_r remove_if_exists(TgBot::Message::Ptr msg);
    query_r remove_if_exists(db_conn& db, TgBot::Message::Ptr msg);

    query_r set_name(TgBot::Message::Ptr msg);
    query_r set_name(db_conn &db, TgBot::Message::Ptr msg);

    query_r account(TgBot::Message::Ptr msg);
    query_r account(db_conn &db, TgBot::Message::Ptr msg);

    query_r balance(TgBot::Message::Ptr msg);
    query_r balance(db_conn &db, TgBot::Message::Ptr msg);

    query_r update_balance(TgBot::Message::Ptr msg);
    query_r update_balance(db_conn& db, TgBot::Message::Ptr msg);
};


#endif //PFANDKASTEN_USERS_HPP
