//
// Created by gesus on 10.03.18.
//

#ifndef PFANDKASTEN_BOX_HPP
#define PFANDKASTEN_BOX_HPP

#include <string>

#include <tgbot/tgbot.h>

#include <database/connection.hpp>
#include <helpers/query_r.hpp>

namespace box {
    query_r deposit(TgBot::Message::Ptr msg);
    query_r deposit(db_conn &db, TgBot::Message::Ptr msg);

    query_r contains(const std::string &drink);
    query_r contains(db_conn &db, const std::string &drink);

    query_r withdraw_if_exists(TgBot::Message::Ptr msg);
    query_r withdraw_if_exists(db_conn &db, TgBot::Message::Ptr msg);

    query_r list(TgBot::Message::Ptr msg);
    query_r list(db_conn& db, TgBot::Message::Ptr msg);
};

#endif //PFANDKASTEN_BOX_HPP
