//
// Created by gesus on 08.03.18.
//

#ifndef PFANDKASTEN333_BOT_HPP
#define PFANDKASTEN333_BOT_HPP

#include <string>

#include <tgbot/tgbot.h>

class TgBot_wrapper : public TgBot::Bot {
private:
    TgBot_wrapper();

    bool is_cmd(const std::string &msgText);
    std::vector<std::string> commands;

public:
    explicit TgBot_wrapper(std::string token);
    void start();

    TgBot_wrapper& set_on_cmd(const std::string &cmd, const TgBot::EventBroadcaster::MessageListener& listener);
    TgBot_wrapper& set_on_noncmd_msg(const TgBot::EventBroadcaster::MessageListener& listener);
};


#endif //PFANDKASTEN333_BOT_HPP
