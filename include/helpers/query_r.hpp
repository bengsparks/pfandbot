//
// Created by gesus on 10.03.18.
//

#ifndef PFANDKASTEN_QUERY_R_HPP
#define PFANDKASTEN_QUERY_R_HPP

#include <utility>

#include "db_common.hpp"

class query_r {
private:
    const exit_code_e   __m_exit_code;
    const std::string   __m_exit_result;

public:
    query_r() = delete;
    query_r(const exit_code_e exit_code, std::string exit_result) :
            __m_exit_code(exit_code),
            __m_exit_result(std::move(exit_result))
    { }

    auto get_exit_code() -> decltype(auto) {
        return __m_exit_code;
    }

    auto get_exit_result() -> decltype(auto) {
        return __m_exit_result;
    }
};

#endif //PFANDKASTEN_QUERY_R_HPP
