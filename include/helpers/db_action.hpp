//
// Created by gesus on 23.03.18.
//

#ifndef PFANDBOT_DB_ACTION_HPP
#define PFANDBOT_DB_ACTION_HPP

#include <cinttypes>
#include <string>
#include <optional>


class db_action {
public:
    class builder;

    db_action(std::optional<int64_t> __m_uid, std::optional<uint64_t> __m_value,
              std::optional<int64_t> __m_quantity, std::optional<uint64_t> __m_valdiff,
              std::optional<std::string> __m_pname, std::optional<std::string> __m_action) :
            __m_uid(std::move(__m_uid)), __m_quantity(std::move(__m_quantity)), __m_value(std::move(__m_value)),
            __m_valdiff(std::move(__m_valdiff)), __m_pname(std::move(__m_pname)), __m_action(std::move(__m_action)) {}

private:
    const std::optional<int64_t> __m_uid, __m_quantity;
    const std::optional<uint64_t> __m_value, __m_valdiff;
    const std::optional<std::string> __m_pname, __m_action;

public:
    decltype(__m_uid) get_uid() const { return __m_uid; }
    decltype(__m_value) get_value() const { return __m_value; }
    decltype(__m_quantity) get_quantity() const { return __m_quantity; }
    decltype(__m_valdiff) get_valdiff() const { return __m_valdiff; }
    decltype(__m_pname) get_pname() const { return __m_pname; }
    decltype(__m_action) get_action() const { return __m_action; }

    std::string to_str() const;
};


class db_action::builder {
private:
    std::optional<int64_t> __m_uid, __m_quantity;
    std::optional<uint64_t> __m_value, __m_valdiff;
    std::optional<std::string> __m_pname, __m_action;

public:
    builder() = default;

    builder& uid(int64_t uid) {
        this->__m_uid = uid; return *this;
    }
    builder& pname(const std::string& pname) {
        this->__m_pname = pname; return *this;
    }
    builder& action(const std::string& action) {
        this->__m_action = action; return *this;
    }
    builder& value(uint64_t value) {
        this->__m_value = value; return *this;
    }
    builder& quantity(int64_t quantity) {
        this->__m_quantity = quantity; return *this;
    }
    builder& valdiff(uint64_t valdiff) {
        this->__m_valdiff = valdiff; return *this;
    }
    const db_action build() {
        return db_action(
                this->__m_uid, this->__m_value, this->__m_quantity,
                this->__m_valdiff, this->__m_pname, this->__m_action
        );
    }
};


#endif //PFANDBOT_DB_ACTION_HPP
