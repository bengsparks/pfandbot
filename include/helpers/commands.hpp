//
// Created by gesus on 12.03.18.
//

#ifndef PFANDKASTEN_COMMANDS_HPP
#define PFANDKASTEN_COMMANDS_HPP


#include <string>
#include <map>
#include <vector>


using kw_lookup = std::map<std::string, std::string>;

auto kw_to_str(const std::pair<kw_lookup::key_type, const kw_lookup::mapped_type> &elem) -> std::string;


namespace commands {
    class __flag_key_pair;

    kw_lookup parse_flags(const std::string &text, const std::vector<__flag_key_pair> &fkps);
    const std::string fmt(const std::string &cmd);

    const std::string __extract_flag_value(const std::string &str, const std::string &flag, std::string::size_type from,
                                           std::string::size_type to);
    const unsigned long __find_position(const std::string &needle, const std::string &haystack);

    namespace misc {
        static auto constexpr ping = []() { return "ping"; };
    }

    namespace user {
        static auto constexpr account = []() { return "user_account"; };
        static auto constexpr balance = []() { return "user_balance"; };
        static auto constexpr record = []() { return "start"; };
        static auto constexpr setname = []() { return "user_setname"; };
        static auto constexpr remove = []() { return "user_remove"; };
    };


    namespace box {
        static auto constexpr deposit = []() { return "box_deposit"; };
        static auto constexpr withdraw = []() { return "box_withdraw"; };
        static auto constexpr list = []() { return "box_list"; };
    };


    namespace pfand {
        static auto constexpr record = []() { return "pfand_record"; };
        static auto constexpr remove = []() { return "pfand_remove"; };
        static auto constexpr list = []() { return "pfand_list"; };
    };


    class __flag_key_pair : public std::pair<std::string, std::string> {
    public:
        __flag_key_pair() = delete;
        __flag_key_pair(std::string flag, std::string key) :
                std::pair<std::string, std::string>(flag, key)
        { }

        auto flag() -> decltype(auto) { return first; }
        auto key() -> decltype(auto) { return second; }

        auto flag() const -> decltype(auto) { return first; }
        auto key() const -> decltype(auto) { return second; }
    };
};


#endif //PFANDKASTEN_COMMANDS_HPP
