//
// Created by gesus on 10.03.18.
//

#ifndef PFANDKASTEN_COMMON_HPP
#define PFANDKASTEN_COMMON_HPP

#include <numeric>
#include <map>
#include <vector>


typedef enum {
    SUCCESS,
    USER_DOES_EXIST, USER_DOES_NOT_EXIST,
    POORLY_FMT_CMD,

    ITEM_IN_TBL,
    ITEM_DUPL_IN_TBL,
    ITEM_NOT_IN_TBL,
    EMPTY_TBL,

    INDEX_OUT_OF_BOUNDS,

    UNKNOWN_ERROR,
    INVALID_ACTION_MAP
} exit_code_e;


static constexpr auto query_result_size = [](auto& tuples) {
    return std::distance(tuples.begin(), tuples.end());
};


static constexpr auto identity = [](auto elem) -> decltype(auto) {
    return elem;
};



static constexpr auto join_on_token = [](const std::string& token, auto fmt) {
    return [&](std::string& accum, auto& elem) {
        return accum.empty() ? fmt(elem) : accum + token + fmt(elem);
    };
};


auto split(const std::string& str, const std::string& delim) -> std::vector<std::string>;
auto split(const std::string& str, const char* delim) -> std::vector<std::string>;


auto remove_extra_ws(const std::string& text) -> std::string;


#endif //PFANDKASTEN_COMMON_HPP
