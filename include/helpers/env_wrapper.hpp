//
// Created by gesus on 14.03.18.
//

#ifndef PFANDBOT_ENV_WRAPPER_HPP
#define PFANDBOT_ENV_WRAPPER_HPP

#include <string>

const std::string safe_getenv(const char *env_name);

#endif //PFANDBOT_ENV_WRAPPER_HPP
